//a. The number of bits used to represent a char value 

class Q_10a
{
    public static void main(String args[])
    {
         System.out.println("size : "+Character.SIZE);
        System.out.println("byte : "+Character.BYTES);
        System.out.println("min_value : "+Character.MIN_VALUE);
        System.out.println("max_value : "+Character.MAX_VALUE);
    }
}