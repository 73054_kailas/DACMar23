// no. of bits used to represent a byte value

class Q_7a
{
    public static void main(String args[])
    {
        System.out.println("no.of bits used to represent byte value(size) : "+Byte.SIZE);
        System.out.println("no.of bytes used to represent byte value(byte) : "+Byte.BYTES);
        System.out.println("min value of byte(min_value) : "+Byte.MIN_VALUE);
        System.out.println("max value of a byte(max_value) : "+Byte.MAX_VALUE);
    }
}