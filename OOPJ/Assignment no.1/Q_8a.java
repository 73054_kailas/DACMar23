// convert byte value to string

class Q_8a
{
    public static void main(String args[])
    {
        Byte b = 102;
        String str = Byte.toString(b);
        System.out.println("string value is "+str);
    }
}