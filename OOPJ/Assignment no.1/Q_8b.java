// convert byte value into byte instance

class Q_8b
{
    public static void main(String args[])
    {
        Byte b = 102;
        Byte b1 = Byte.valueOf(b);
        System.out.println("byte instance is "+b1);
    }
}