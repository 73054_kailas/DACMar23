class Q_9
{
    public static void main(String args[])
    {
        byte b = 101;
        Byte b1 = Byte.valueOf(b);
        System.out.println("byte :"+b1);
       
       
        byte b2 = 102;
        Byte b3 = new Byte(b2);
       short sh = b3.shortValue();
        System.out.println("short :"+sh);

        int b4 = b3.intValue();
         System.out.println("int :"+b4);

         long l = b3.longValue();
         System.out.println("long :"+l);

         float f =b3.floatValue();
         System.out.println("float :"+f);

         double d = b3.doubleValue();
         System.out.println("double :"+d);


        
    }
}